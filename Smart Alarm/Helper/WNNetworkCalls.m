//
//  WNNetworkCalls.m
//  whitenoise
//
//  Created by Subham Khandelwal on 24/03/16.
//  Copyright © 2016 master. All rights reserved.
//

#import "WNNetworkCalls.h"
#import <AFHTTPSessionManager.h>

@implementation WNNetworkCalls

- (void)makePostRequestWithURL: (NSString *) url
                     andParams: (NSDictionary *) requestParams
                    withHeader:(NSDictionary *)headers
                       success:(void (^)(NSDictionary *))success
                       failure:(void (^)(id))failure

{
    __block NSDictionary *apiResponse;
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithArray:@[@"application/javascript", @"application/json"]];
    manager.responseSerializer.acceptableStatusCodes = [NSIndexSet indexSetWithIndex:400];
    manager.responseSerializer.acceptableStatusCodes = [NSIndexSet indexSetWithIndex:200];
    
    NSArray *allKeys = [headers allKeys];
    for (NSString *eachHeaderKey in allKeys)
        [manager.requestSerializer setValue:[headers valueForKey:eachHeaderKey] forHTTPHeaderField:eachHeaderKey];
    
    [manager POST:url parameters:requestParams progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        apiResponse = responseObject;
        success(apiResponse);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSString *errorString = @"";
//        if (error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey]) {
//            NSDictionary *errorResponse = [NSJSONSerialization JSONObjectWithData:error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey]
//                                                                          options:NSJSONReadingMutableContainers
//                                                                            error:nil];
//            NSLog(@"errror: %@", errorResponse);
//        }
        if (NSSTRING_HAS_DATA([error.userInfo valueForKey:@"NSLocalizedDescription"]))
            errorString = [error.userInfo valueForKey:@"NSLocalizedDescription"];
        failure(errorString);
    }];
}

- (void)makeGetRequestWithURL: (NSString *) url
                    andParams: (NSDictionary *) requestParams
                   withHeader:(NSDictionary *)headers
                      success:(void (^)(NSDictionary *successResponse))success
                      failure:(void (^)(id failureResponse))failure

{
    
    __block NSDictionary *apiResponse;
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithArray:@[@"application/javascript", @"application/json"]];
    manager.responseSerializer.acceptableStatusCodes = [NSIndexSet indexSetWithIndex:400];
    manager.responseSerializer.acceptableStatusCodes = [NSIndexSet indexSetWithIndex:200];
    
    NSArray *allKeys = [headers allKeys];
    for (NSString *eachHeaderKey in allKeys)
        [manager.requestSerializer setValue:[headers valueForKey:eachHeaderKey] forHTTPHeaderField:eachHeaderKey];
    
    [manager GET:url parameters:requestParams progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        apiResponse = responseObject;
        success(apiResponse);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSString *errorString = @"";
        
//        if (error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey]) {
//            NSDictionary *errorResponse = [NSJSONSerialization JSONObjectWithData:error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey]
//                                                                          options:NSJSONReadingMutableContainers
//                                                                            error:nil];
//            NSLog(@"errror: %@", errorResponse);
//        }
        if (NSSTRING_HAS_DATA([error.userInfo valueForKey:@"NSLocalizedDescription"]))
            errorString = [error.userInfo valueForKey:@"NSLocalizedDescription"];
        
        failure(errorString);
    }];
}

@end
