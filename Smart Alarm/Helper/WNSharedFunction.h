//
//  WNSharedFunction.h
//  whitenoise
//
//  Created by Subham Khandelwal on 22/03/16.
//  Copyright © 2016 master. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface WNSharedFunction : NSObject

+ (WNSharedFunction *) sharedInstance;
- (CGSize) calculateSizeOfLabel:(UILabel *)lable withMaxWidth:(int)width;
- (void)createAlertViewWithTitle: (NSString *)titleString andMessage: (NSString *)message withpresentingVC:(id)delegate withHandler:(void(^)(void))handler;

@end
