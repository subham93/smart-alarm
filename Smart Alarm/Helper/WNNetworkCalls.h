//
//  WNNetworkCalls.h
//  whitenoise
//
//  Created by Subham Khandelwal on 24/03/16.
//  Copyright © 2016 master. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WNNetworkCalls : NSObject

- (void)makePostRequestWithURL: (NSString *) url
                     andParams: (NSDictionary *) requestParams
                    withHeader:(NSDictionary *)headers
                       success:(void (^)(NSDictionary *))success
                       failure:(void (^)(id))failure;

- (void)makeGetRequestWithURL: (NSString *) url
                    andParams: (NSDictionary *) requestParams
                   withHeader:(NSDictionary *)headers
                      success:(void (^)(NSDictionary *successResponse))success
                      failure:(void (^)(id failureResponse))failure;

@end
