//
//  WNSharedFunction.m
//  whitenoise
//
//  Created by Subham Khandelwal on 22/03/16.
//  Copyright © 2016 master. All rights reserved.
//

#import "WNSharedFunction.h"

@implementation WNSharedFunction

+ (WNSharedFunction *) sharedInstance
{
    static WNSharedFunction *sharedInstance = nil;
    @synchronized(self)
    {
        if (sharedInstance == nil)
        {
            sharedInstance = [[WNSharedFunction alloc] init];
        }
    }
    return sharedInstance;
}

- (CGSize) calculateSizeOfLabel:(UILabel *)lable withMaxWidth:(int)width
{
    
    if (!NSSTRING_HAS_DATA(lable.text))
    {
        return CGSizeMake(0, 10);
    }
    //Calculate the expected size based on the font and linebreak mode of your label
    
    CGSize maximumLabelSize = CGSizeMake(width, CGFLOAT_MAX);
    CGSize expectedLabelSize;
    
    NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                          lable.font, NSFontAttributeName,
                                          nil];
    
    CGRect frame = [lable.text boundingRectWithSize:maximumLabelSize
                                            options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading
                                         attributes:attributesDictionary
                                            context:nil];
    frame.size.height = ceil(frame.size.height);
    frame.size.width  = ceil(frame.size.width);
    expectedLabelSize = frame.size;
    return expectedLabelSize;
}

- (void)createAlertViewWithTitle: (NSString *)titleString andMessage: (NSString *)message withpresentingVC:(id)delegate withHandler:(void(^)(void))handler
{
    if ([UIAlertController class])
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:titleString message:message preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                                   {
                                       if (handler)
                                           handler();
                                   }];
        if (handler)
        {
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
            [alertController addAction:cancelAction];
        }
        [alertController addAction:okAction];
        
        if ([delegate isKindOfClass:[UIViewController class]])
            [(UIViewController *)delegate presentViewController:alertController animated:YES completion:nil];
        else
            [(UINavigationController *)delegate pushViewController:alertController animated:YES];
    }
    else
    {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:titleString message:message delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
}

@end
