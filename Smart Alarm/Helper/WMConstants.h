//
//  WMConstants.h
//  whitenoise
//
//  Created by Subham Khandelwal on 20/03/16.
//  Copyright © 2016 master. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WMConstants : NSObject

#define APP_BASE_COLOR [UIColor colorWithRed:55/255.0 green:190/255.0 blue:198/255.0 alpha:1]
#define UBUNTU_LIGHT_12 [UIFont fontWithName:@"Ubuntu-Light" size:12]
#define NSSTRING_HAS_DATA(_x) ( ((_x) != nil) && ([(_x)length] > 0) )

#define MENU_URL    @"https://apps.masterappsolutions.com:8443/apps_by_master/api/apps/menu/en/BabyJukebox"
#define ABOUT_URL   @"https://apps.masterappsolutions.com:8443/apps_by_master/api/apps/about/en/BabyJukebox"

#define MODE_TITLE_ARR  @[@"Full Mode", @"Set time, sleep cycle & noise recording mode", @"Sleep cycle mode", @"Sleep Quality Control", @"Custom Mode", @"Power nap mode"]

#define MODE_IMG_SET  @[@[@"ic_righttime", @"ic_monitoring", @"ic_microphone"], @[@"ic_settime", @"ic_monitoring", @"ic_microphone"], @[@"ic_righttime", @"ic_monitoring", @"ic_microphone_unactive"], @[@"ic_settime_unactive", @"ic_monitoring", @"ic_microphone"], @[@"", @"ic_custom", @""], @[@"ic_beforedeepsleep",@"ic_monitoring", @"ic_microphone_unactive"]]

@end
