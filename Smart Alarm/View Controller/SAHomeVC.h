//
//  SAHomeVC.h
//  Smart Alarm
//
//  Created by Subham Khandelwal on 28/03/16.
//  Copyright © 2016 master. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {MODE, TIMER, CLOCK} SCREEN_TYPE;
typedef enum {ALARM, STATISTICS, MUSIC, HELP, SETTINGS, ABOUT, RATE, SHARE} FEATURES;

@interface SAHomeVC : UIViewController <UIPageViewControllerDataSource, UIPageViewControllerDelegate>

@property (strong, nonatomic) UIPageViewController *pageController;

@end
