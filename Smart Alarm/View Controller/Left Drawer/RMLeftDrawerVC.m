//
//  RMLeftDrawerVC.m
//  Relax Melodies
//
//  Created by Subham Khandelwal on 07/03/16.
//  Copyright © 2016 master. All rights reserved.
//

#import "RMLeftDrawerVC.h"
#import "WNNetworkCalls.h"
#import "SAHomeVC.h"
#import "AppDelegate.h"

@interface RMLeftDrawerVC ()

@property (weak, nonatomic) IBOutlet UITableView *leftDrawerTableview;
@property (strong, nonatomic) NSMutableArray *otherAppsArr;

@end

static NSString *cellId = @"leftCellId";
@implementation RMLeftDrawerVC

- (void)viewDidLoad {
    
    [super viewDidLoad];

    [self.view setBackgroundColor:APP_BASE_COLOR];
    _otherAppsArr = [NSMutableArray array];
    
    [self getOtherApps];
}

- (void)getOtherApps {
    
    WNNetworkCalls *networkCall = [[WNNetworkCalls alloc] init];
    [networkCall makeGetRequestWithURL:MENU_URL andParams:nil withHeader:nil success:^(NSDictionary *successResponse) {
        for (NSDictionary *eachApp in [[successResponse valueForKey:@"apps"] firstObject])
            [_otherAppsArr addObject:eachApp];
        [_leftDrawerTableview reloadData];
    } failure:^(id failureResponse) {
        
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 2;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 44;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    switch (section) {
        case 0:
            return 7;
            break;
        case 1:
            return [_otherAppsArr count];
            break;
        default:
            break;
    }
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        cell.backgroundColor = [UIColor blackColor];
        cell.textLabel.textColor = [UIColor whiteColor];
        cell.textLabel.font = [UIFont fontWithName:@"Ubuntu" size:14];
    }
    
    switch (indexPath.section)
    {
        case 0:
        {
            switch (indexPath.row)
            {
                case ALARM:
                    cell.textLabel.text = @"Alarm";
                    cell.imageView.image = [UIImage imageNamed:@"ic_alarm"];
                    break;
                case STATISTICS:
                    cell.textLabel.text = @"Statistics";
                    cell.imageView.image = [UIImage imageNamed:@"ic_monitoring"];
                    break;
                case MUSIC:
                    cell.textLabel.text = @"Music";
                    cell.imageView.image = [UIImage imageNamed:@"ic_music"];
                    break;
                case HELP:
                    cell.textLabel.text = @"Help";
                    cell.imageView.image = [UIImage imageNamed:@"ic_help"];
                    break;
                case SETTINGS:
                    cell.textLabel.text = @"Settings";
                    cell.imageView.image = [UIImage imageNamed:@"ic_custom"];
                    break;
                case ABOUT:
                    cell.textLabel.text = @"About";
                    cell.imageView.image = [UIImage imageNamed:@"ic_about"];
                    break;
                case RATE:
                    cell.textLabel.text = @"Rate us";
                    cell.imageView.image = [UIImage imageNamed:@"ic_menu_rate_us"];
                    break;
                case SHARE:
                    cell.textLabel.text = @"Share this app";
                    cell.imageView.image = [UIImage imageNamed:@"ic_menu_shareapp"];
                    break;
                default:
                    break;
            }
        }
            break;
        case 1:
        {
            if ([_otherAppsArr count] > indexPath.row)
            {
                NSDictionary *otherAppDict = _otherAppsArr[indexPath.row];
                cell.textLabel.text = [otherAppDict valueForKey:@"name"];
                cell.imageView.image = nil;// [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[otherAppDict valueForKey:@"iconUrl"]]]];
            }
        }
            break;
        default:
            break;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    switch (indexPath.section) {
        case 0:
        {
            switch (indexPath.row)
            {
//                case RADIO:
//                    [[self getHomeVC] showHomePage];
//                    break;
//                case RATTLE:
//                    [[self getHomeVC] showRattlePage];
//                    break;
//                case TIPS:
//                    [[self getHomeVC] showBabyTipsPage];
//                    break;
//                case TIMER:
//                    [[self getHomeVC] showSoundTimerPage];
//                    break;
//                case LOG:
//                    [[self getHomeVC] showLogPage];
//                    break;
//                case ABOUT:
//                    [[self getHomeVC] showAboutPage];
//                    break;
//                default:
//                    break;
            }
            break;
        }
        case 1:
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[_otherAppsArr[indexPath.row] valueForKey:@"storeUrl"]]];
            break;
        default:
            break;
    }
}

- (SAHomeVC *)getHomeVC
{
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    UINavigationController *navVC = (UINavigationController *)appDel.drawerController.centerViewController;
    return (SAHomeVC *)navVC.visibleViewController;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
