//
//  SAStatisticsVC.m
//  Smart Alarm
//
//  Created by Subham Khandelwal on 05/04/16.
//  Copyright © 2016 master. All rights reserved.
//

#import "SAStatisticsVC.h"
#import "SAStatisticsCell.h"

@interface SAStatisticsVC ()

@property (weak, nonatomic) IBOutlet UITableView *statsTableView;

@end

static NSString *cellId = @"statisticCell";

@implementation SAStatisticsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [_statsTableView registerNib:[UINib nibWithNibName:@"SAModeSelectCell" bundle:nil] forCellReuseIdentifier:cellId];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 60;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 10;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
    
    return cell;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
