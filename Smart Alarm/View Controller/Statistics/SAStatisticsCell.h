//
//  SAStatisticsCell.h
//  Smart Alarm
//
//  Created by Subham Khandelwal on 05/04/16.
//  Copyright © 2016 master. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SAStatisticsCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *durationLabel;
@property (weak, nonatomic) IBOutlet UILabel *spanLabel;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;

- (void)setDate:(NSString *)dateText
    setDuration:(NSString *)durationText
        setSpan:(NSString *)spanText
       setCount:(NSString *)count;

@end
