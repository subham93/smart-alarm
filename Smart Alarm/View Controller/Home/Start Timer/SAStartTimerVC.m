//
//  SAStartTimerVC.m
//  Smart Alarm
//
//  Created by Subham Khandelwal on 28/03/16.
//  Copyright © 2016 master. All rights reserved.
//

#import "SAStartTimerVC.h"
#import "DatePicker.h"
#import "AppDelegate.h"

@interface SAStartTimerVC () <DatePickerDelegate>

@property (weak, nonatomic) IBOutlet UIView *switchModeView;

@property (assign, nonatomic) NSInteger timerHour;
@property (assign, nonatomic) NSInteger timerMins;

@property (weak, nonatomic) IBOutlet UIImageView *firstImgView;
@property (weak, nonatomic) IBOutlet UIImageView *secondImgView;
@property (weak, nonatomic) IBOutlet UIImageView *thirdImgView;
@property (weak, nonatomic) IBOutlet UILabel *modeLabel;

@end


@implementation SAStartTimerVC
{
    DatePicker *datePicker;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor blackColor]];
    self.timerHour = 1;
    self.timerMins = 0;
    
    UIButton *hamburgerMenuItem = [[UIButton alloc] initWithFrame:CGRectMake(10, 10, 30, 30)];
    [hamburgerMenuItem setTitle:@"|||" forState:UIControlStateNormal];
    [hamburgerMenuItem addTarget:self action:@selector(toggleLeftDrawer:) forControlEvents:UIControlEventTouchUpInside];
    [hamburgerMenuItem setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.view addSubview:hamburgerMenuItem];
    
    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gotoSwitchModePage:)];
    [_switchModeView addGestureRecognizer:gesture];
    
    UIView *rightLine = [[UIView alloc] initWithFrame:CGRectMake(-2, 0, 2, self.view.frame.size.height)];
    [rightLine setBackgroundColor:[UIColor orangeColor]];
    [self.view addSubview:rightLine];
}

-(void)viewDidLayoutSubviews
{
    datePicker = [[DatePicker alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-120, 200, 240, 240)];
    [datePicker setDelegate:self];
    [datePicker setBackgroundColor:[UIColor blackColor]];
    [self.view addSubview:datePicker];
    [datePicker calculateTimeAndSendToDelegate];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [_modeLabel setText:MODE_TITLE_ARR[_modeImgIndex]];
    [_firstImgView setImage:[UIImage imageNamed:MODE_IMG_SET[_modeImgIndex][0]]];
    [_secondImgView setImage:[UIImage imageNamed:MODE_IMG_SET[_modeImgIndex][1]]];
    [_thirdImgView setImage:[UIImage imageNamed:MODE_IMG_SET[_modeImgIndex][2]]];

    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    [delegate.drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
    [delegate.drawerController setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeAll];
}

- (void)toggleLeftDrawer:(id)sender {
    
    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    [delegate.drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}

- (void)gotoSwitchModePage:(id)sender {

    if (_delegate && [_delegate respondsToSelector:@selector(switchModeClcicked:)]) {
        [_delegate switchModeClcicked:sender];
    }
}

- (IBAction)startTimerClicked:(id)sender {
    
    [datePicker calculateTimeAndSendToDelegate];
    if (_delegate && [_delegate respondsToSelector:@selector(startTimerClickedWithHours:withMinutes:)]) {
        [_delegate startTimerClickedWithHours:self.timerHour withMinutes:self.timerMins];
    }
}

-(void)timeChanged:(NSDate *)time {
    
    NSLog(@"Time: %@", time);

    [self getTimeDiffFromCurrentTimeOf:time];
}

- (void)getTimeDiffFromCurrentTimeOf:(NSDate *)time
{
    NSDate *someDateInUTC = [NSDate date];
    NSTimeInterval timeZoneSeconds = [[NSTimeZone localTimeZone] secondsFromGMT];
    NSDate *dateInLocalTimezone = [someDateInUTC dateByAddingTimeInterval:timeZoneSeconds];
    
    NSTimeInterval diffTime = [time timeIntervalSinceDate:dateInLocalTimezone];
    if (diffTime > 0)
    {
        NSLog(@"H: %f|| M: %d", diffTime/3600, (int)diffTime%3600/60);
        _timerHour = (NSInteger)diffTime/3600;
        _timerMins = (NSInteger)diffTime%3600/60;
    }
    else
        [self getTimeDiffFromCurrentTimeOf:[time dateByAddingTimeInterval:3600*24]];
}

- (IBAction)noteButtonClicked:(id)sender {
    
    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    [delegate.drawerController toggleDrawerSide:MMDrawerSideRight animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
