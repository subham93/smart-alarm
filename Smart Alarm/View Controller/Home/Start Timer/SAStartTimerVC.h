//
//  SAStartTimerVC.h
//  Smart Alarm
//
//  Created by Subham Khandelwal on 28/03/16.
//  Copyright © 2016 master. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol startTimer <NSObject>

- (void)startTimerClickedWithHours:(NSInteger)hours withMinutes:(NSInteger)minutes;
- (void)switchModeClcicked:(id)sender;

@end

@interface SAStartTimerVC : UIViewController

@property (weak, nonatomic) id delegate;
@property (assign, nonatomic) NSInteger modeImgIndex;

@end
