//
//  SAModeSelectVC.h
//  Smart Alarm
//
//  Created by Subham Khandelwal on 28/03/16.
//  Copyright © 2016 master. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol modeSelected <NSObject>

- (void)modeSelected:(NSInteger)selectedMode;

@end

typedef enum {FULL_MODE, TIME_CYCLE_NOISE_MODE, SLEEP_CYCLE_MODE, QUALITY_CONTROL_MODE, CUSTOM_MODE, POWER_NAP_MODE}SLEEP_MODES;

@interface SAModeSelectVC : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) id delegate;

@end
