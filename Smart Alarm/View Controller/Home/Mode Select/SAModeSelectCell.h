//
//  SAModeSelectCell.h
//  Smart Alarm
//
//  Created by Subham Khandelwal on 28/03/16.
//  Copyright © 2016 master. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SAModeSelectCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *checkMarkImgView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subtitleLabel;

- (void)setTitle:(NSString *)title andSubtitle:(NSString *)subtitle andImageSet:(NSArray *)imgSet;

@end
