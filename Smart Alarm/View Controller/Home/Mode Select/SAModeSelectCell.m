//
//  SAModeSelectCell.m
//  Smart Alarm
//
//  Created by Subham Khandelwal on 28/03/16.
//  Copyright © 2016 master. All rights reserved.
//

#import "SAModeSelectCell.h"

@interface SAModeSelectCell()

@property (weak, nonatomic) IBOutlet UIImageView *rightTimeImgView;
@property (weak, nonatomic) IBOutlet UIImageView *monitoringImgView;
@property (weak, nonatomic) IBOutlet UIImageView *microphoneImgView;
@property (weak, nonatomic) IBOutlet UIImageView *tickImgView;

@end

@implementation SAModeSelectCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setTitle:(NSString *)title andSubtitle:(NSString *)subtitle andImageSet:(NSArray *)imgSet {

    [_titleLabel setText:title];
    [_subtitleLabel setText:subtitle];
    [_rightTimeImgView setImage:[UIImage imageNamed:imgSet[0]]];
    [_monitoringImgView setImage:[UIImage imageNamed:imgSet[1]]];
    [_microphoneImgView setImage:[UIImage imageNamed:imgSet[2]]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
