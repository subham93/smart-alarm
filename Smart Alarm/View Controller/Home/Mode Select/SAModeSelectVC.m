//
//  SAModeSelectVC.m
//  Smart Alarm
//
//  Created by Subham Khandelwal on 28/03/16.
//  Copyright © 2016 master. All rights reserved.
//

#import "SAModeSelectVC.h"
#import "SAModeSelectCell.h"
#import "AppDelegate.h"

@interface SAModeSelectVC ()

@property (weak, nonatomic) IBOutlet UITableView *modeTableView;
@property (assign, nonatomic) NSInteger selectedIndex;

@end

static NSString *cellIdentifier = @"modeCellId";

@implementation SAModeSelectVC
{
    NSArray *titleArr;
    NSArray *subtitleArr;
    NSArray *imgSet;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    titleArr = @[@"Full Mode", @"Set time, sleep cycle & noise recording mode", @"Sleep cycle mode", @"Sleep Quality Control", @"Custom Mode", @"Power nap mode"];
    subtitleArr = @[@"Monitors sleep cycle and records disturbances during the night. Wakes you up at the right time to avoid under or oversleeping.",
                    @"Monitors sleep cycle and records disturbances during the night. Wakes you up exactly at the time that you set beforehand.",
                    @"Monitors sleep cycle. Wakes you up at the right time to avoid under or oversleeping.",
                    @"Monitors sleep cycle and records disturbances during the night. Doesn't wake you up in the morning.",
                    @"Lets you adjust Smart Alarm Clock to your own needs and preferences.",
                    @"Monitors sleep cycle to wake you up before you enter deep sleep, but no later than your time ends. Power naps less than 45 minutes help you recover and feel energetic the rest of the day"];
    
    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    [delegate.drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeNone];

    [_modeTableView registerNib:[UINib nibWithNibName:@"SAModeSelectCell" bundle:nil] forCellReuseIdentifier:cellIdentifier];
    _modeTableView.rowHeight = UITableViewAutomaticDimension;

    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    return 30;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [titleArr count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    SAModeSelectCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    [cell setTitle:MODE_TITLE_ARR[indexPath.row] andSubtitle:subtitleArr[indexPath.row] andImageSet:MODE_IMG_SET[indexPath.row]];
    if (_selectedIndex == indexPath.row)
        [cell.checkMarkImgView setImage:[UIImage imageNamed:@"ic_check_active"]];
    else
        [cell.checkMarkImgView setImage:[UIImage imageNamed:@"ic_check_unactive"]];

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    _selectedIndex = indexPath.row;
    [tableView reloadData];
    if (_delegate && [_delegate respondsToSelector:@selector(modeSelected:)]) {
        [_delegate modeSelected:indexPath.row];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
