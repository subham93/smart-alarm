//
//  SAClockVC.h
//  Smart Alarm
//
//  Created by Subham Khandelwal on 28/03/16.
//  Copyright © 2016 master. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol clockStopped <NSObject>

- (void)clockStopped:(id)sender;

@end

@interface SAClockVC : UIViewController

@property (weak, nonatomic) id<clockStopped> delegate;
@property (assign, nonatomic) NSInteger timerHour;
@property (assign, nonatomic) NSInteger timerMins;

@end
