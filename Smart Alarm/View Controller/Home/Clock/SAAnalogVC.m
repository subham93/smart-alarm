//
//  SAAnalogVC.m
//  Smart Alarm
//
//  Created by Subham Khandelwal on 03/04/16.
//  Copyright © 2016 master. All rights reserved.
//

#import "SAAnalogVC.h"

@interface SAAnalogVC () <BEMAnalogClockDelegate>

@property (nonatomic,strong) NSDateFormatter *dateFormatter;
@property (nonatomic,strong) NSCalendar *calendar;
@property (nonatomic,strong) NSDate *date;

@end

@implementation SAAnalogVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.myClock1.enableShadows = YES;
    self.myClock1.realTime = YES;
    self.myClock1.currentTime = YES;
    self.myClock1.setTimeViaTouch = NO;
    self.myClock1.borderColor = [UIColor blackColor];
    self.myClock1.borderWidth = 3;
    self.myClock1.faceBackgroundColor = [UIColor whiteColor];
    self.myClock1.faceBackgroundAlpha = 1.0;
    self.myClock1.delegate = self;
    self.myClock1.digitFont = [UIFont fontWithName:@"HelveticaNeue-Thin" size:17];
    self.myClock1.digitColor = [UIColor blackColor];
    self.myClock1.enableDigit = YES;
    
    self.myClock1.hourHandColor = [UIColor blackColor];
    self.myClock1.hourHandOffsideLength = 0;
    self.myClock1.hourHandLength = 40;

    self.myClock1.minuteHandColor = [UIColor blackColor];
    self.myClock1.minuteHandOffsideLength = 0;
    self.myClock1.minuteHandLength = 70;
    self.myClock1.minuteHandWidth = 2;
    
    self.myClock1.secondHandColor = [UIColor orangeColor];
    self.myClock1.secondHandOffsideLength = 0;
    self.myClock1.secondHandLength = 70;

    
//    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
//    panGesture.delegate = self;
//    [panGesture setMaximumNumberOfTouches:1];
//    [self.panView addGestureRecognizer:panGesture];
}

- (CGFloat)analogClock:(BEMAnalogClockView *)clock graduationLengthForIndex:(NSInteger)index {
//    if (clock.tag == 1) {
        if (!(index % 5) == 1) { // Every 5 graduation will be longer.
            return 15;
        } else {
            return 5;
        }
//    }
//    else return 0;
}

- (UIColor *)analogClock:(BEMAnalogClockView *)clock graduationColorForIndex:(NSInteger)index {
    if (!(index % 15) == 1) { // Every 15 graduation will be blue.
        return [UIColor blueColor];
    } else {
        return [UIColor grayColor];
    }
}

- (void)currentTimeOnClock:(BEMAnalogClockView *)clock Hours:(NSString *)hours Minutes:(NSString *)minutes Seconds:(NSString *)seconds {
    if (clock.tag == 1) {
        int hoursInt = [hours intValue];
        int minutesInt = [minutes intValue];
        int secondsInt = [seconds intValue];
    }
}

- (void)handlePan:(UIPanGestureRecognizer *)recognizer {
    CGPoint translation = [recognizer locationInView:self.view];
    //    self.myClock1.minutes = translation.x / 5.33333;
    
    float minutes = translation.x/2.666667;  // 320 width / 2.666667 = 120 minutes [2 hours]
    
    if (!_dateFormatter){
        _dateFormatter = [[NSDateFormatter alloc] init];
        [_dateFormatter setDateFormat:@"HH:mm:ss"];
        _calendar = [NSCalendar currentCalendar];
//        _date = [_dateFormatter dateFromString:self.myLabel.text];
    }
    NSDate           *datePlusMinutes = [_date dateByAddingTimeInterval:minutes*60];
    NSDateComponents *components      = [_calendar components:(NSHourCalendarUnit | NSMinuteCalendarUnit) fromDate:datePlusMinutes];
    NSInteger hour   = [components hour];
    NSInteger minute = [components minute];
    
    self.myClock1.minutes = minute;
    self.myClock1.hours   = hour;
    
    [self.myClock1 updateTimeAnimated:NO];
}

- (IBAction)pushRefreshButton:(id)sender {
    self.myClock1.hours = arc4random() % 12;
    self.myClock1.minutes = arc4random() % 60;
    self.myClock1.seconds = arc4random() % 60;
    [self.myClock1 updateTimeAnimated:YES];
}

- (IBAction)pushCurrentTimeButton:(id)sender {
    if (self.myClock1.realTimeIsActivated == NO) {
        [self.myClock1 startRealTime];
    } else {
        [self.myClock1 stopRealTime];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
