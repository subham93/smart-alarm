//
//  SATimerLeftVC.m
//  Smart Alarm
//
//  Created by Subham Khandelwal on 04/04/16.
//  Copyright © 2016 master. All rights reserved.
//

#import "SATimerLeftVC.h"

@interface SATimerLeftVC ()

@property (weak, nonatomic) IBOutlet UILabel *timerLeftLabel;
@property (strong, nonatomic) IBOutlet UIView *timerLeftClockView;

@end

@implementation SATimerLeftVC
{
    NSTimer *aTimer;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [_timerLeftClockView.layer setCornerRadius:100];
    [_timerLeftClockView.layer setBorderWidth:1];
    [_timerLeftClockView.layer setBorderColor:[UIColor darkGrayColor].CGColor];
    
    aTimer = [NSTimer scheduledTimerWithTimeInterval:60 target:self selector:@selector(updateTimerLabel) userInfo:nil repeats:YES];

}

- (void)updateTimerLabel
{
    if (_timerMins > 0) {
        _timerMins--;
    }
    else {
        if (_timerHour > 0) {
            _timerHour--;
            _timerMins = 59;
        }
        else {
            _timerHour = 0;
            _timerMins = 0;
            [aTimer invalidate];
        }
    }
    
    [_timerLeftLabel setText:[NSString stringWithFormat:@"%ld : %ld", (long)_timerHour, (long)_timerMins]];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [_timerLeftLabel setText:[NSString stringWithFormat:@"%ld : %ld", (long)_timerHour, (long)_timerMins]
     ];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
