//
//  SAAnalogVC.h
//  Smart Alarm
//
//  Created by Subham Khandelwal on 03/04/16.
//  Copyright © 2016 master. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <BEMAnalogClock/BEMAnalogClockView.h>

@interface SAAnalogVC : UIViewController

@property (weak, nonatomic) IBOutlet BEMAnalogClockView *myClock1;

@end
