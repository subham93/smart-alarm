//
//  SATimerLeftVC.h
//  Smart Alarm
//
//  Created by Subham Khandelwal on 04/04/16.
//  Copyright © 2016 master. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SATimerLeftVC : UIViewController

@property (assign, nonatomic) NSInteger timerHour;
@property (assign, nonatomic) NSInteger timerMins;

@end
