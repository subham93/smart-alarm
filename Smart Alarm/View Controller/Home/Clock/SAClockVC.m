//
//  SAClockVC.m
//  Smart Alarm
//
//  Created by Subham Khandelwal on 28/03/16.
//  Copyright © 2016 master. All rights reserved.
//

#import "SAClockVC.h"
#import "SAAnalogVC.h"
#import "AppDelegate.h"
#import "SATimerLeftVC.h"

@interface SAClockVC () <UIPageViewControllerDataSource>

@property (strong, nonatomic) UIPageViewController *pageController;
@property (assign, nonatomic) NSInteger currentPageIndex;

@end

@implementation SAClockVC

- (void)viewDidLoad {
    [super viewDidLoad];

    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    [delegate.drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeNone];

    [[UIPageControl appearance] setPageIndicatorTintColor: [UIColor darkGrayColor]];
    [[UIPageControl appearance] setCurrentPageIndicatorTintColor: [UIColor whiteColor]];
    [[UIPageControl appearance] setBackgroundColor: [UIColor blackColor]];

    self.pageController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    
    self.pageController.dataSource = self;
    CGRect frame = self.view.bounds;
    frame.size.height -= 60;
    [self.pageController.view setFrame:frame];
    _currentPageIndex = 0;
    SAAnalogVC *initialViewController = (SAAnalogVC *)[self viewControllerAtIndex:_currentPageIndex];
    
    NSArray *viewControllers = [NSArray arrayWithObject:initialViewController];
    
    [self.pageController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    [self addChildViewController:self.pageController];
    [self.view addSubview:self.pageController.view];
    [self.pageController didMoveToParentViewController:self];
}

-(void)gotoPageIndex:(NSInteger)index
{
    if (_currentPageIndex > index)
        [self.pageController setViewControllers:[NSArray arrayWithObject:[self viewControllerAtIndex:index]] direction:UIPageViewControllerNavigationDirectionReverse animated:YES completion:nil];
    else
        [self.pageController setViewControllers:[NSArray arrayWithObject:[self viewControllerAtIndex:index]] direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    
    if ([viewController isKindOfClass:[SATimerLeftVC class]])
    {
        return nil;
    }
    else if ([viewController isKindOfClass:[SAAnalogVC class]])
    {
        SATimerLeftVC *timerLeftVC = [[SATimerLeftVC alloc] initWithNibName:@"SATimerLeftVC" bundle:nil];
        timerLeftVC.timerHour = _timerHour;
        timerLeftVC.timerMins = _timerMins;
        return timerLeftVC;
    }
    
    return nil;
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    
    if ([viewController isKindOfClass:[SATimerLeftVC class]])
    {
        SAAnalogVC *analogVC = [[SAAnalogVC alloc] initWithNibName:@"SAAnalogVC" bundle:nil];
        return analogVC;
    }
    else if ([viewController isKindOfClass:[SAAnalogVC class]])
    {
        return nil;
    }
    
    return nil;
}

- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray<UIViewController *> *)previousViewControllers transitionCompleted:(BOOL)completed
{
    if ([pageViewController isKindOfClass:[SATimerLeftVC class]])
        _currentPageIndex = 0;
    else if ([pageViewController isKindOfClass:[SAAnalogVC class]])
        _currentPageIndex = 1;
}

- (UIViewController *)viewControllerAtIndex:(NSUInteger)index {
    
    if (index >= 2) {
        return nil;
    }
    else if (index == 0) {
        SATimerLeftVC *timerLeftVC = [[SATimerLeftVC alloc] initWithNibName:@"SATimerLeftVC" bundle:nil];
        timerLeftVC.timerHour = _timerHour;
        timerLeftVC.timerMins = _timerMins;
        return timerLeftVC;
    }
    else if (index == 1) {
        SAAnalogVC *analogVC = [[SAAnalogVC alloc] initWithNibName:@"SAAnalogVC" bundle:nil];
        return analogVC;
    }

    return [[UIViewController alloc] init];
    
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController {
    // The number of items reflected in the page indicator.
    return 2;
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController {
    // The selected item reflected in the page indicator.
    return 0;
}

- (IBAction)stopTimerClock:(id)sender {
    
    if (_delegate && [_delegate respondsToSelector:@selector(clockStopped:)]) {
        [_delegate clockStopped:sender];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
