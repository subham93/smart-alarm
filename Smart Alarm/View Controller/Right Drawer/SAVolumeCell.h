//
//  SAVolumeCell.h
//  Smart Alarm
//
//  Created by Subham Khandelwal on 30/03/16.
//  Copyright © 2016 master. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SAVolumeCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UISlider *volumeSlider;
@property (weak, nonatomic) id delegate;

@end
