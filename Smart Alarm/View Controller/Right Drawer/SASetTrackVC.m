//
//  SASetTrackVC.m
//  Smart Alarm
//
//  Created by Subham Khandelwal on 04/04/16.
//  Copyright © 2016 master. All rights reserved.
//

#import "SASetTrackVC.h"

@interface SASetTrackVC ()

@property (weak, nonatomic) IBOutlet UITableView *tracksTableView;
@property (weak, nonatomic) IBOutlet UIButton *iPodButton;
@property (weak, nonatomic) IBOutlet UIButton *tracksButton;

@end

@implementation SASetTrackVC
{
    UIView *bottomLine;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Set Track";
    bottomLine = [[UIButton alloc] initWithFrame:CGRectMake(0, 29, 140, 1)];
    [bottomLine setBackgroundColor:[UIColor blueColor]];
}

- (IBAction)trackButtonClicked:(id)sender {
    [_tracksButton addSubview:bottomLine];
}

- (IBAction)iPodButtonClicked:(id)sender {
    [_iPodButton addSubview:bottomLine];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 2;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {

    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, _tracksTableView.frame.size.width, 30)];
    if (section == 0)
    {
        [headerLabel setTextColor:[UIColor orangeColor]];
        [headerLabel setText:@"More Music!"];
    }
    else if (section == 1)
    {
        [headerLabel setTextColor:[UIColor whiteColor]];
        [headerLabel setText:@"Built-in Tracks!"];
    }
    return headerLabel;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 30;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (section == 1)
        return 10;
    else
        return 0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"hyperCell"];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"hyperCell"];
        [cell.contentView setBackgroundColor:[UIColor blackColor]];
        [cell.textLabel setTextColor:[UIColor whiteColor]];
        [cell.textLabel setFont:UBUNTU_LIGHT_12];
    }
    cell.textLabel.text = @"test";
    
    return cell;
}

@end
