//
//  SARightDrawerVC.m
//  Smart Alarm
//
//  Created by Subham Khandelwal on 30/03/16.
//  Copyright © 2016 master. All rights reserved.
//

#import "SARightDrawerVC.h"
#import "SADisclosureCell.h"
#import "SAVolumeCell.h"
#import "SASetTrackVC.h"

@interface SARightDrawerVC ()
@property (weak, nonatomic) IBOutlet UITableView *musicTableView;
@property (assign, nonatomic) NSInteger musicDuration;

@end

static NSString *setTrackCellId = @"setTrackCell";
static NSString *timeMusicCellId = @"timeMusicCell";
static NSString *volumeCellId = @"volumeCellId";

@implementation SARightDrawerVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Music";
    
    _musicDuration = 10;
    
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:[UIFont fontWithName:@"Ubuntu" size:14]}];

    [_musicTableView registerNib:[UINib nibWithNibName:@"SADisclosureCell" bundle:nil] forCellReuseIdentifier:setTrackCellId];
    [_musicTableView registerNib:[UINib nibWithNibName:@"SAVolumeCell" bundle:nil] forCellReuseIdentifier:volumeCellId];
    [_musicTableView setSeparatorColor:[UIColor lightGrayColor]];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 44;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 30;
}

- (CGFloat)tableView:(UITableView*)tableView heightForFooterInSection:(NSInteger)section {
    return 30;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UILabel *headerLbl = [[UILabel alloc] initWithFrame:CGRectMake(30, 0, self.view.frame.size.width-30, 30)];
    [headerLbl setTextColor:[UIColor blueColor]];
    if (section == 0)
        [headerLbl setText:@"Wake-up music"];
    else if (section == 1)
        [headerLbl setText:@"Chill-out music"];
    
    return headerLbl;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    
    UIView *footerView = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 30)];
    [footerView setBackgroundColor:[UIColor blackColor]];
    
    return footerView;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (section == 0) {
        return 1;
    } else if (section == 1) {
        return 3;
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0)
    {
        SADisclosureCell *cell = [tableView dequeueReusableCellWithIdentifier:setTrackCellId];
        [cell setDisclosureLabelText:@"A New Day"];
        return cell;
    }
    if (indexPath.section == 1)
    {
        if (indexPath.row == 0)
        {
            SADisclosureCell *cell = [tableView dequeueReusableCellWithIdentifier:setTrackCellId];
            [cell setDisclosureLabelText:@"Cascades"];
            return cell;
        } else if (indexPath.row == 1)
        {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:timeMusicCellId];
            if (cell == nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:timeMusicCellId];
                cell.textLabel.text = @"Duration";
                cell.textLabel.textColor = [UIColor whiteColor];
                cell.textLabel.font = [UIFont fontWithName:@"Ubuntu-Light" size:14];
                [cell setBackgroundColor:[UIColor blackColor]];
            }
            
            [cell.detailTextLabel setAttributedText:[self getDurationTextForMins:_musicDuration]];
            return cell;
        } else if (indexPath.row == 2)
        {
            SAVolumeCell *cell = [tableView dequeueReusableCellWithIdentifier:volumeCellId];
            return cell;
        }
    }
    
    return [[UITableViewCell alloc] init];;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.row == 0) {
        SASetTrackVC *setTrackVC = [[SASetTrackVC alloc] init];
        [self.navigationController pushViewController:setTrackVC animated:YES];
    }
    if (indexPath.section == 1 && indexPath.row == 1) {
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        _musicDuration += 10;
        if (_musicDuration >= 100) {
            _musicDuration = 10;
        }
        [cell.detailTextLabel setAttributedText:[self getDurationTextForMins:_musicDuration]];
    }
}

- (NSMutableAttributedString *)getDurationTextForMins:(NSInteger)mins
{
    NSMutableAttributedString *text =
    [[NSMutableAttributedString alloc]
     initWithAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%ldmins", mins]]];
    
    [text addAttribute:NSForegroundColorAttributeName
                 value:[UIColor redColor]
                 range:NSMakeRange(2, 4)];
    
    return text;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
