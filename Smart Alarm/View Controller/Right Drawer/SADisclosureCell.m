//
//  SADisclosureCell.m
//  Smart Alarm
//
//  Created by Subham Khandelwal on 30/03/16.
//  Copyright © 2016 master. All rights reserved.
//

#import "SADisclosureCell.h"

@interface SADisclosureCell()

@property (weak, nonatomic) IBOutlet UILabel *disclosureLabel;

@end

@implementation SADisclosureCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setDisclosureLabelText:(NSString *)title
{
    _disclosureLabel.text = title;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
