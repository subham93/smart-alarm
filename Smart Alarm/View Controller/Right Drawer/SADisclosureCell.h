//
//  SADisclosureCell.h
//  Smart Alarm
//
//  Created by Subham Khandelwal on 30/03/16.
//  Copyright © 2016 master. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SADisclosureCell : UITableViewCell

- (void)setDisclosureLabelText:(NSString *)title;

@end
