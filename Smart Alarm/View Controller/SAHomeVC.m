//
//  SAHomeVC.m
//  Smart Alarm
//
//  Created by Subham Khandelwal on 28/03/16.
//  Copyright © 2016 master. All rights reserved.
//

#import "SAHomeVC.h"
#import "SAModeSelectVC.h"
#import "SAStartTimerVC.h"
#import "SAClockVC.h"

@interface SAHomeVC () <startTimer, modeSelected, clockStopped>

@property (assign, nonatomic) NSInteger currentPageIndex;
@property (assign, nonatomic) NSInteger timerHour;
@property (assign, nonatomic) NSInteger timerMins;
@property (assign, nonatomic) NSInteger modeSelectedIndex;

@end

@implementation SAHomeVC
{
    SAModeSelectVC *modeVC;
    SAStartTimerVC *timerVC;
    SAClockVC *clockVC;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.pageController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationVertical options:nil];
    
    self.pageController.dataSource = self;
    [self.pageController.view setFrame:self.view.bounds];
    _currentPageIndex = 1;
    SAStartTimerVC *initialViewController = (SAStartTimerVC *)[self viewControllerAtIndex:_currentPageIndex];
    
    NSArray *viewControllers = [NSArray arrayWithObject:initialViewController];
    
    [self.pageController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    [self addChildViewController:self.pageController];
    [self.view addSubview:self.pageController.view];
    [self.pageController didMoveToParentViewController:self];
}

-(void)gotoPageIndex:(NSInteger)index
{
    if (_currentPageIndex > index)
        [self.pageController setViewControllers:[NSArray arrayWithObject:[self viewControllerAtIndex:index]] direction:UIPageViewControllerNavigationDirectionReverse animated:YES completion:nil];
    else
        [self.pageController setViewControllers:[NSArray arrayWithObject:[self viewControllerAtIndex:index]] direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
    
    _currentPageIndex = index;
}

//- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
//    
//    if ([viewController isKindOfClass:[SAModeSelectVC class]])
//    {
//        return nil;
//    }
//    else if ([viewController isKindOfClass:[SAStartTimerVC class]])
//    {
//        SAModeSelectVC *modeVC = [[SAModeSelectVC alloc] initWithNibName:@"SAModeSelectVC" bundle:nil];
//        modeVC.delegate = self;
//        return modeVC;
//    }
//    else if ([viewController isKindOfClass:[SAClockVC class]])
//    {
//        SAStartTimerVC *timerVC = [[SAStartTimerVC alloc] initWithNibName:@"SAStartTimerVC" bundle:nil];
//        timerVC.delegate = self;
//        return timerVC;
//    }
//    
//    return nil;
//}
//
//- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
//
//    if ([viewController isKindOfClass:[SAModeSelectVC class]])
//    {
//        SAStartTimerVC *timerVC = [[SAStartTimerVC alloc] initWithNibName:@"SAStartTimerVC" bundle:nil];
//        timerVC.delegate = self;
//        return timerVC;
//    }
//    else if ([viewController isKindOfClass:[SAStartTimerVC class]])
//    {
//        SAClockVC *clockVC = [[SAClockVC alloc] initWithNibName:@"SAClockVC" bundle:nil];
//        clockVC.delegate = self;
//        clockVC.timerHour = _timerHour;
//        clockVC.timerMins = _timerMins;
//        return clockVC;
//    }
//    else if ([viewController isKindOfClass:[SAClockVC class]])
//    {
//        return nil;
//    }
//    
//    return nil;
//}

- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray<UIViewController *> *)previousViewControllers transitionCompleted:(BOOL)completed
{
    if ([pageViewController isKindOfClass:[SAModeSelectVC class]])
        _currentPageIndex = 0;
    else if ([pageViewController isKindOfClass:[SAStartTimerVC class]])
        _currentPageIndex = 1;
    else if ([pageViewController isKindOfClass:[SAClockVC class]])
        _currentPageIndex = 2;
}

- (UIViewController *)viewControllerAtIndex:(NSUInteger)index {
    
    if (index >= 3) {
        return nil;
    }
    
    switch (index) {
        case MODE:
        {
            if (!modeVC) {
                modeVC = [[SAModeSelectVC alloc] initWithNibName:@"SAModeSelectVC" bundle:nil];
            }
            modeVC.delegate = self;
            return modeVC;
        }
            break;
        case TIMER:
        {
            if (!timerVC) {
                timerVC = [[SAStartTimerVC alloc] initWithNibName:@"SAStartTimerVC" bundle:nil];
            }
            timerVC.delegate = self;
            timerVC.modeImgIndex = _modeSelectedIndex;
            return timerVC;
        }
            break;
        case CLOCK:
        {
            if (!clockVC) {
                clockVC = [[SAClockVC alloc] initWithNibName:@"SAClockVC" bundle:nil];
            }
            clockVC.delegate = self;
            clockVC.timerHour = _timerHour;
            clockVC.timerMins = _timerMins;
            return clockVC;
        }
            break;
        default:
            break;
    }
    return [[UIViewController alloc] init];
    
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController {
    // The number of items reflected in the page indicator.
    return 3;
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController {
    // The selected item reflected in the page indicator.
    return 0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)modeSelected:(NSInteger)selectedMode
{
    _modeSelectedIndex = selectedMode;
    [self gotoPageIndex:1];
}

- (void)startTimerClickedWithHours:(NSInteger)hours withMinutes:(NSInteger)minutes;
{
    self.timerHour = hours;
    self.timerMins = minutes;
    [self gotoPageIndex:2];
}

- (void)switchModeClcicked:(id)sender
{
    [self gotoPageIndex:0];
}


- (void)clockStopped:(id)sender
{
    [self gotoPageIndex:1];
}

@end
