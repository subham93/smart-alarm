//
//  DatePicker.h
//  PickerControl
//
//  Created by Subham Khandelwal on 27/03/16.
//  Copyright © 2016 Picker. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DatePickerDelegate <NSObject>
@optional
-(void)timeChanged:(NSDate *)time;
@end

@interface DatePicker : UIView

@property(nonatomic,weak)id<DatePickerDelegate>delegate;

- (void)calculateTimeAndSendToDelegate;

@end
