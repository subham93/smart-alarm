//
//  DatePicker.m
//  PickerControl
//
//  Created by Subham Khandelwal on 27/03/16.
//  Copyright © 2016 Picker. All rights reserved.
//

#import "DatePicker.h"
#import "CustomPicker.h"

@interface DatePicker()<CustomPickerDelegate>
@property(nonatomic,strong)NSString *hour;
@property(nonatomic,strong)NSString *minute;
@property(nonatomic,strong)NSString *meridiam;
@end

@implementation DatePicker
{
    UIButton *amMeridiamButton;
    UIButton *pmMeridiamButton;
}

-(id) initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        [self setup];
    }
    return self;
}

-(id)initWithFrame:(CGRect)frame {
    
    if (self = [super initWithFrame:frame]) {
        [self setup];
    }
    return self;
}

-(void)setup {
    
//    self.hour = @"1";
//    self.minute = @"00";
    self.meridiam = @"am";
    
    CustomPicker *hourPicker = [[CustomPicker alloc] initWithFrame:CGRectMake(0,20, self.bounds.size.width/2.5, self.bounds.size.height-40)];
    [hourPicker setType:PickerModeHour];
    [hourPicker setDelegate:self];
    [hourPicker setCurrentHour];
    [self addSubview:hourPicker];
    
    UILabel *colonLabel = [[UILabel alloc] initWithFrame:CGRectMake(hourPicker.frame.size.width-10, 20, 16, hourPicker.frame.size.height)];
    [colonLabel setTextColor:[UIColor whiteColor]];
    [colonLabel setBackgroundColor:[UIColor blackColor]];
    [colonLabel setText:@":"];
    [colonLabel setTextAlignment:NSTextAlignmentCenter];
    [colonLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:colonLabel.frame.size.height/4]];
  
    
    CustomPicker *mintutePick = [[CustomPicker alloc] initWithFrame:CGRectMake(hourPicker.frame.origin.x+hourPicker.frame.size.width, hourPicker.frame.origin.y, hourPicker.frame.size.width , hourPicker.frame.size.height)];
    [mintutePick setType:PickerModeMinute];
    [mintutePick setDelegate:self];
    [mintutePick setCurrentMins];
    [self addSubview:mintutePick];
    
    CGFloat width = self.bounds.size.width - (hourPicker.frame.size.width+mintutePick.frame.size.width);
    
//    CustomPicker *meridiamPick = [[CustomPicker alloc] initWithFrame:CGRectMake(mintutePick.frame.origin.x+mintutePick.frame.size.width/1.5,  (self.bounds.size.height - (width*2))/2, hourPicker.frame.size.width/2, width*2)];
//    [meridiamPick setType:PickerModeMeridiem];
//    [meridiamPick setDelegate:self];
//    [self addSubview:meridiamPick];
    amMeridiamButton = [[UIButton alloc] initWithFrame:CGRectMake(mintutePick.frame.origin.x+mintutePick.frame.size.width/1.5+20,  self.frame.size.height/2-width-10, hourPicker.frame.size.width/2, width)];
    [amMeridiamButton addTarget:self action:@selector(amMeridiamBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    amMeridiamButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    // you probably want to center it
    amMeridiamButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    [amMeridiamButton setTitle:@"am" forState: UIControlStateNormal];
    [self addSubview:amMeridiamButton];
    
    pmMeridiamButton = [[UIButton alloc] initWithFrame:CGRectMake(mintutePick.frame.origin.x+mintutePick.frame.size.width/1.5+20,  CGRectGetMaxY(amMeridiamButton.frame)+10, hourPicker.frame.size.width/2, width)];
    [pmMeridiamButton addTarget:self action:@selector(pmMeridiamBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    pmMeridiamButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    // you probably want to center it
    pmMeridiamButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    [pmMeridiamButton setTitle:@"pm" forState: UIControlStateNormal];
    [self addSubview:pmMeridiamButton];
    [self amMeridiamBtnClicked:nil];

    [self addSubview:colonLabel];
}

- (void)amMeridiamBtnClicked:(id)sender
{
    self.meridiam = @"am";
    [amMeridiamButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [pmMeridiamButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [self calculateTimeAndSendToDelegate];
}

- (void)pmMeridiamBtnClicked:(id)sender
{
    self.meridiam = @"pm";
    [amMeridiamButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [pmMeridiamButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self calculateTimeAndSendToDelegate];
    
}

-(void)scrollStopsWithValue:(NSString *)val ofType:(PickerMode)type {
    
    if (type == PickerModeHour) {
        self.hour = val;
    }else if(type == PickerModeMinute) {
        self.minute = val;
    }else if(type == PickerModeMeridiem) {
        self.meridiam = val;
    }
    
    [self calculateTimeAndSendToDelegate];
}

- (void)calculateTimeAndSendToDelegate
{
    NSDate *oldDate = [NSDate date];
    unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay;
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *comps = [calendar components:unitFlags fromDate:oldDate];
    if ([self.hour integerValue] == 12)
        comps.hour   = [self.meridiam isEqualToString:@"pm"] ? [self.hour integerValue] : [self.hour integerValue] - 12;
    else
        comps.hour   = [self.meridiam isEqualToString:@"am"] ? [self.hour integerValue] : [self.hour integerValue] + 12;
    comps.minute = [self.minute integerValue];
    [comps setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    
    NSDate *newDate = [calendar dateFromComponents:comps];
    
    if ([self.delegate respondsToSelector:@selector(timeChanged:)]) {
        [self.delegate timeChanged:newDate];
    }
}

@end
