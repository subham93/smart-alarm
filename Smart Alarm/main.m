//
//  main.m
//  Smart Alarm
//
//  Created by Subham Khandelwal on 28/03/16.
//  Copyright © 2016 master. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
